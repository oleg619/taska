﻿using System.ComponentModel.DataAnnotations;

namespace CarsTest.Models
{
    public class File
    {
        [StringLength(255)]
        public string FileName { get; set; }
        [StringLength(100)]
        public string ContentType { get; set; }
        public byte[] Content { get; set; }
    }
}