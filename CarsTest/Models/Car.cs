﻿using System.ComponentModel.DataAnnotations;

namespace CarsTest.Models
{
    public class Car
    {
        private int _carId;
        private string _model;
        private int _year;      
        private string _imagePath;
        [Required]
        [StringLength(32, ErrorMessage = "Model cannot be longer than 32 characters.")]
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        [Required]
        [Range(1950, 2017, ErrorMessage = "Range must be between 1950 and 2017")]
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        public string ImagePath
        {
            get { return _imagePath; }
            set { _imagePath = value; }
        }

        [Key]
        public int CarId
        {
            get { return _carId; }
            private set { _carId = value; }
        }

    }
}