﻿using System.Collections.Generic;
using CarsTest.Models;

namespace CarsTest.DAL
{
    public class CarsInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CarsContext>
    {
        protected override void Seed(CarsContext context)
        {
            var cars = new List<Car>
            {
                new Car{Model = "Mers", Year = 2000, ImagePath = "Content/Images/merc.jpg"},
                new Car{Model = "BMW", Year = 2002, ImagePath = "Content/Images/bmw.jpg"},
                new Car{Model = "Lanos", Year = 2008, ImagePath = "Content/Images/lanos.jpg"},
                new Car{Model = "Audi", Year = 2008, ImagePath = "Content/Images/audi.jpg"},
            };

            cars.ForEach(c => context.Cars.Add(c));
            context.SaveChanges();
        }
    }
}