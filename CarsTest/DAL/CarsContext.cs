﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using CarsTest.Models;

namespace CarsTest.DAL
{
    public class CarsContext : DbContext
    {
        public CarsContext() : base("CarsContext")
        {
        }
        public DbSet<Car> Cars { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}