﻿using System.Web.Mvc;
using CarsTest.Controllers;
using CarsTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CarTest.Test
{
    [TestClass]
    public class TestController
    {
        [TestMethod]
        public void TestIndex()
        {
            var controller = new CarController();
            var res = controller.Index() as ViewResult;
            Assert.IsNotNull(res);
        }

        [TestMethod]
        public void TestDetailsViewData()
        {
            var controller = new CarController();
            var result = controller.Details(2) as ViewResult;
            if (result != null)
            {
                var car = (Car)result.ViewData.Model;
                Assert.AreEqual("BMW", car.Model);
            }
        }
    }
}
